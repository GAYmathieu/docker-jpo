<?php


use PHPUnit\Framework\TestCase;

require 'Functions.php';

class FunctionsTest extends TestCase {

    public function testAddDataToCsv() {
        $testCases = [
            [
                'description' => 'Valid data',
                'data' => [
                    'nom' => 'Faye',
                    'prenom' => 'Sarah',
                    'telephone' => '0734343434',
                    'email' => 's.faye@ipssi.com',
                    'formation' => 'Informatique',
                    'options1' => ['Bac +2', 'BTS SIO'],
                    'options2' => ['Réseaux sociaux'],
                    'date' => '2022-01-01'
                ],
                'expected' => true
            ],
            [
                'description' => 'Empty input',
                'data' => [
                    'nom' => '',
                    'prenom' => '',
                    'telephone' => '',
                    'email' => '',
                    'formation' => '',
                    'options1' => [],
                    'options2' => [],
                    'date' => ''
                ],
                'expected' => false
            ],
            [
                'description' => 'Special characters in name',
                'data' => [
                    'nom' => 'Faye@',
                    'prenom' => 'Sarah@',
                    'telephone' => '0734343434',
                    'email' => 's.faye@ipssi.com',
                    'formation' => 'Informatique',
                    'options1' => ['Bac +2', 'BTS SIO'],
                    'options2' => ['Réseaux sociaux'],
                    'date' => '2022-01-01'
                ],
                'expected' => false
            ],
            [
                'description' => 'Special characters in phone',
                'data' => [
                    'nom' => 'Faye ',
                    'prenom' => 'Sarah',
                    'telephone' => '@0734343434.@',
                    'email' => 's.faye@ipssi.com',
                    'formation' => 'Informatique',
                    'options1' => ['Bac +2', 'BTS SIO'],
                    'options2' => ['Réseaux sociaux'],
                    'date' => '2022-01-01'
                ],
                'expected' => false
            ],
            [
                'description' => 'Invalid email',
                'data' => [
                    'nom' => 'Faye',
                    'prenom' => 'Sarah',
                    'telephone' => '0734343434',
                    'email' => 's.fayeipssi.com',
                    'formation' => 'Informatique',
                    'options1' => ['Bac +2', 'BTS SIO'],
                    'options2' => ['Réseaux sociaux'],
                    'date' => '2022-01-01'
                ],
                'expected' => false
            ]
        ];

        foreach ($testCases as $testCase) {
            $result = addDataToCsv($testCase['data']);
            $this->assertEquals($testCase['expected'], $result, $testCase['description']);
        }
    }
}
