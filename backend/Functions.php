<?php

function isValidEmail($email) {
    $pattern = "/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/";
    $matches = [];
    if (preg_match($pattern, $email, $matches)) {
        return $matches[0] === $email; 
    }
    return false;
}

function addDataToCsv($data) {
    $allowedCharactersName = '/^[a-zA-Z\s]*$/'; 
    $allowedCharactersPhone = '/^\d+$/';

    if (!preg_match($allowedCharactersName, $data['nom']) || !preg_match($allowedCharactersPhone, $data['telephone'])) {
        return false;
    }

    if (!isValidEmail($data['email'])) {
        return false;
    }

    $directory = './csvFiles/';
    $fileName = 'PreInscription-'. $data['date']. '.csv';
    $filePath = $directory. $fileName;

    if (!file_exists($directory)) {
        mkdir($directory, 0777, true);
    }

    $fp = fopen($filePath, 'a');
    if ($fp === false) {
        return false;
    }

    $headers = ['Nom', 'Prénom', 'Téléphone', 'Email', 'Formation', 'interessé par', "j'ai connu l'ipssi grâce", "Date"];
    $dataRow = [
        $data['nom'],
        $data['prenom'],
        $data['telephone'],
        $data['email'],
        $data['formation'],
        implode('|', $data['options1']),
        implode('|', $data['options2']),
        $data['date']
    ];

    if (filesize($filePath) === 0) {
        fputcsv($fp, $headers);
    }
    fputcsv($fp, $dataRow);

    fclose($fp);

    return true;
}
