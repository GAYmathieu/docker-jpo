<?php

// En-têtes pour autoriser les requêtes CORS
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    http_response_code(200);
    exit();
}

// Répertoire où se trouvent les fichiers CSV
$directory = 'csvFiles';

// Permet de lister les fichiers dans le répertoire
$files = scandir(__DIR__ . '/' . $directory);

// Permet la suppression du chemin parent et courant
$files = array_diff($files, array('..', '.'));

$csvFiles = [];

foreach ($files as $file) {
  
  // Extrait la date du nom du fichier
  $date = substr($file, 15,10);

  $csvFiles[] = [
    'date' => $date,
    'download_link' => $directory . '/' . $file,
    'filename' => $file
  ];
}

header('Content-Type: application/json');
echo json_encode($csvFiles);