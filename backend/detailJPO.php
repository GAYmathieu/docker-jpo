<?php

// En-têtes pour autoriser les requêtes CORS
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    http_response_code(200);
    exit();
}

$filename = $_GET['filename'];

// Répertoire où se trouvent les fichiers CSV
$directory = 'csvFiles';

// Chemin du fichier
$filepath = __DIR__ . '/' . $directory . '/' . $filename;

$data = [];

// Ouvre le fichier en lecture
$file = fopen($filepath, 'r');

// Si le fichier n'existe pas, stop le script
if ($file === false) {
    die('Unable to open file');
}

// Avec la fonction fgetcsv, on lis le fichier ligne par ligne
while (($row = fgetcsv($file)) !== false) {
    $data[] = $row;
}

// Ferme le fichier en lecture
fclose($file);

// Supprime l'entête
$header = array_shift($data);

header('Content-Type: application/json');
echo json_encode($data);