# Application

## Prérequis
Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre machine :

- PHP 8.1
- Composer
- Node.js

## Clonez le projet

```bash
git clone https://gitlab.com/GAYmathieu/docker-jpo
cd docker-jpo
```

## Lancer l'application localement
### Backend

Accédez au dossier backend :

```bash
cd backend
```

Installez les dépendances :
```bash
composer install
```

Démarrez le serveur :
```bash
php -S localhost:8001
```

### Frontend

Vous pouvez accéder au site via: localhost:80

## Démarrer les tests
### Tests Frontend

Accédez au dossier frontend :
```bash
cd frontend
```

Installez les dépendances :
```bash
npm install
```

Exécutez les tests :
```bash
npm run test
```

### Tests Backend

Assurez-vous que le serveur backend est en cours d'exécution.

Accédez au dossier backend :
```bash
cd backend
Exécutez les tests :
```

```bash
vendor/bin/phpunit FunctionsTest.php
```

## Lancer l'application avec Docker

Placez-vous à la racine du projet.

Téléchargez les images nécessaires :

```bash
docker-compose pull
```

Lancez l'application :
```bash
docker-compose up
```

Attendez que le téléchargement des images soit terminé avant d'exécuter docker-compose up.

Vous pouvez accéder au site via: localhost:80

## Deployez vous même

### Front:

Créez un fichier Dockerfile (si il n'existe pas) et collez y la configuration suivante:

```yaml
# Utiliser l'image httpd
FROM httpd:latest

# Copier le contenu du répertoire front dans le répertoire htdocs de httpd
COPY . /usr/local/apache2/htdocs/


# Configuration de ServerName pour supprimer l'avertissement
RUN echo 'ServerName localhost' >> /usr/local/apache2/conf/httpd.conf

# Workdir
WORKDIR /usr/local/apache2/htdocs/

# Installer node
RUN apt-get update && apt-get install -y nodejs npm

# Exposer le port 80
EXPOSE 80

# Lancer le serveur httpd
CMD ["httpd-foreground"]
```

### Backend

Créez un fichier Dockerfile (si il n'existe pas) et collez y la configuration suivante:
```yaml
# Utiliser l'image PHP avec Apache
FROM php:8.1-apache

# Copier le fichier PHP dans le répertoire de l'Apache
COPY . /var/www/html/

# Configuration de ServerName pour supprimer l'avertissement
RUN echo 'ServerName localhost' >> /etc/apache2/apache2.conf

# Installation de Composer
RUN apt-get update && \
    apt-get install -y \
        git \
        unzip && \
    rm -rf /var/lib/apt/lists/* && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html/

# Exposer le port 8001
EXPOSE 8001

# Lancer le serveur Apache
CMD ["apache2-foreground"]
```

### Gitlab-ci

Configurez votre fichier `.gitlab-ci.yaml` si il n'est pas configué. Ajoutez y les différentes étapes de déploiement (build, test, deploy, ect...). Vous pouvez garder le fichier qui existe de base.

Envoyez ces fichiers vers votre répository avec la commande:

```bash
git add .
git commit -m "Initial commit"
git push
```

Les runners sont préconfigurées pour utiliser ceux de Gitlab. Vous pouvez suivre l'avancement de la publication dans l'onglet `Pipelines` sur Gitlab.

Une fois fini, vous pouvez récupérer vos images dans l'onglet `Container Registery`.