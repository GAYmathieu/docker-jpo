/**
 * @jest-environment jsdom
 */

const fs = require('fs');
const path = require('path');

describe('registrationForm', () => {
    beforeEach(() => {
        document.body.innerHTML = fs.readFileSync(path.resolve(__dirname, '../index.html'), 'utf8');
        require('../app.js'); // chemin vers app

        form = document.getElementById('registrationForm');
        submitEvent = new Event('submit', {
            bubbles: true,
            cancelable: true
        });
    });

    test('should prevent form submission if required fields are empty', () => {
        form.dispatchEvent(submitEvent);

        expect(submitEvent.defaultPrevented).toBe(true);
    });


    test('should submit form if all required fields are filled and "accepte" is checked', async () => {
        const form = document.getElementById('registrationForm');
        document.getElementById('nom').value = 'Test';
        document.getElementById('prenom').value = 'User';
        document.getElementById('telephone').value = '1234567890';
        document.getElementById('email').value = 'test@example.com';
        document.getElementById('formation').value = 'Formation Test';
        document.getElementById('accepte').checked = true;
        document.getElementById('date').value = '2023-01-01';

        global.fetch = jest.fn(() =>
            Promise.resolve({
                json: () => Promise.resolve({ success: true })
            })
        );

        const submitEvent = new Event('submit', {
            bubbles: true,
            cancelable: true
        });

        form.dispatchEvent(submitEvent);

    });

    test('should log message if email is invalid', () => {
        const logSpy = jest.spyOn(console, 'log').mockImplementation(() => { });

        document.getElementById('nom').value = 'Test';
        document.getElementById('prenom').value = 'User';
        document.getElementById('telephone').value = '1234567890';
        document.getElementById('email').value = 'invalid-email';
        document.getElementById('formation').value = 'Formation Test';
        document.getElementById('accepte').checked = true;
        document.getElementById('date').value = '2023-01-01';

        form.dispatchEvent(submitEvent);

        logSpy.mockRestore();
    });

    test('should log message if surname contains special characters', () => {
        const logSpy = jest.spyOn(console, 'log').mockImplementation(() => { });

        document.getElementById('nom').value = 'Test';
        document.getElementById('prenom').value = 'User@';
        document.getElementById('telephone').value = '1234567890';
        document.getElementById('email').value = 'test@example.com';
        document.getElementById('formation').value = 'Formation Test';
        document.getElementById('accepte').checked = true;
        document.getElementById('date').value = '2023-01-01';

        form.dispatchEvent(submitEvent);

        logSpy.mockRestore();
    });



});
