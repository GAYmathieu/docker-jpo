document.getElementById('registrationForm').addEventListener('submit', async function (e) {
    e.preventDefault();

    const nom = document.getElementById('nom').value;
    const prenom = document.getElementById('prenom').value;
    const telephone = document.getElementById('telephone').value;
    const email = document.getElementById('email').value;
    const formation = document.getElementById('formation').value;
    const accepte = document.getElementById('accepte').checked;
    const date = document.getElementById('date').value;

    const getMessage = document.getElementById('error');
    getMessage.innerHTML = '';

    const options1 = Array.from(document.querySelectorAll('input[name="options1[]"]:checked')).map(el => el.value);
    const options2 = Array.from(document.querySelectorAll('input[name="options2[]"]:checked')).map(el => el.value);

    if (!validateEmail(email)) {
        getMessage.innerHTML = 'Veuillez entrer une adresse e-mail valide.';
        getMessage.style.display = 'block';
        window.location.href = '#error';
        return;
    }
    
    if (containsSpecialChars(nom) || containsSpecialChars(prenom) || containsSpecialChars(telephone)) {
        // console.log('Nom, prénom et téléphone ne doivent pas contenir de caractères spéciaux.', error);
        getMessage.innerHTML = 'Nom, prénom et téléphone ne doivent pas contenir de caractères spéciaux.';
        getMessage.style.display = 'block';
        window.location.href = '#error';
        return;
    }

    // limite fixée à 10 chiffres + aucun autres caractères
    if (!(/^\d{10}$/).test(telephone)) {
        // console.log('Le numéro de téléphone doit contenir exactement 10 chiffres et aucun autre caractère.');
        getMessage.innerHTML = 'Le numéro de téléphone doit contenir exactement 10 chiffres et aucun autre caractère.';
        getMessage.style.display = 'block';
        window.location.href = '#error';
        return;
    }


    if (accepte) {
        try {
            const newData = {
                nom,
                prenom,
                telephone,
                email,
                formation,
                options1,
                options2,
                date
            };

            const response = await fetch('http://localhost:8001/saveData.php', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newData)
            });

            const responseData = await response.json();

            document.getElementById('registrationForm').reset();
        } catch (error) {
            console.error('Erreur lors de la soumission des données:', error);
        }
    } else {
        console.log('Veuillez accepter la conservation et l\'utilisation de vos données.');
    }
});

// email avec "@" + "." obligatoire
function validateEmail(email) {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
}

// pas de caractères spéciaux
function containsSpecialChars(str) {
    const re = /[!@#\$%\^\&*\)\(+=._-]/g;
    return re.test(str);
}

// Restriction des dates disponibles
document.addEventListener('DOMContentLoaded', () => {
    const select = document.getElementById('date');
    const allowedDates = [
        { value: '2024-06-10', text: '10 Juin 2024' },
        { value: '2024-06-15', text: '15 Juin 2024' },
        { value: '2024-06-20', text: '20 Juin 2024' }
    ];

    allowedDates.forEach(date => {
        const option = document.createElement('option');
        option.value = date.value;
        option.textContent = date.text;
        select.appendChild(option);
    });
})