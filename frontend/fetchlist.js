
window.addEventListener('DOMContentLoaded', () => {
    const csvList = document.getElementById('csvList');

    fetch('http://localhost:8001/listJPO.php', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      
    })
      .then(response => response.json())
      .then(data => {

        // Trier les donnée par la date JPO
        data.sort((a, b) => {
          return new Date(b.date) - new Date(a.date);
        });

        data.forEach(file => {

          // Créer une ligne dans le tableau pour chaque fichier
          const row = document.createElement('tr');

          // Insère dans la balise tr, les balises td avec les données du fichier
          row.innerHTML = `
            <tr>
              <td>${file.date}</td>
              <td>
                <button onclick='SeeDetails("${file.filename}")'>Voir Plus</button>
              </td>
              <td>
                <a href="http://localhost:8001/${file.download_link}">${file.filename}</a>
              </td>
            </tr>
          `;

          // Ajoute les lignes au tableau
          csvList.appendChild(row);
        });
      })
      .catch(error => {
        console.error("Erreur lors de la soumission des données:", error);
      });
});

// Fonction pour voir les détails d'un fichier
function SeeDetails(filename) {
    fetch(`http://localhost:8001/detailJPO.php?filename=${filename}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => {
            const detailsTable = document.getElementById('detailsTable');

            // Supprimer les anciennes données
            detailsTable.innerHTML = '';

            // Créer une ligne pour chaque donnée (tr)
            data.forEach((row) => {
                const tr = document.createElement('tr');
                row.forEach(col => {

                    // Si la colonne est vide, on affiche "Non renseigné"
                    if (col === "") {
                        col = "Non renseigné";
                    }
                    
                    // Remplace les | par des saut de ligne
                    col = col.replace("|", "\n");

                    const td = document.createElement('td');
                    td.textContent = col;
                    tr.appendChild(td);
                });
                detailsTable.appendChild(tr);
            })
        })
        .catch(error => {
          console.error("Erreur lors de la récupération des détails:", error);
        }
    );
         
    var x = document.getElementById("details-csv");
    x.style.display = "block";
}